import java.util.Scanner;

public class BoardGameApp {
	
	public static void main(String[] args){
				
	//Introduction
		System.out.println("Welcome New Player in my WALL and CASTLE Game!");
		int numCastles = 7;
		int turns = 0;
		
		Board myBoard = new Board();
		Scanner reader = new Scanner(System.in);
		
		//gameloop
		while(numCastles > 0 && turns < 8){
			
			//display board + castle in hand + turn passsed
			System.out.println("\n" + myBoard);
			System.out.print("\nNumber of CASTLES in your Hand: " + numCastles);
			System.out.print("\nTurns n. : " + turns);
			
			//enter pos
			System.out.print("\nEnter a row: ");
			int row = reader.nextInt();
			
			System.out.print("\nEnter a column: ");
			int column = reader.nextInt();
			
			//is position valid? No = retry; Wall = lost turn; Blank = -1 castle
			int isPlaced = myBoard.placeToken(row, column);
			
			if(isPlaced < 0){
				System.out.print("\nPosition Invalid! \nRe-Enter a new Position!");

			}
			else if(isPlaced == 1){
				System.out.print("\nThere was a WALL! \nThis turn was lost!");
				turns++;
			}
			else if(isPlaced == 0){
				System.out.print("\nCASTLE successfully Placed!");
				turns++;
				numCastles--;
			}

			
			
		}
		
		//Game Summary
			System.out.println("\n" + myBoard);
		
		//Winning state
		if(numCastles == 0){
			
		System.out.println("\nYou WON the Game");
		}
		else{
		System.out.println("\nYou LOST the Game");
		}
		



	}
}