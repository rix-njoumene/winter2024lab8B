public enum Tile{
	
	BLANK		(" "),
	WALL		("W"),
	HIDDEN_WALL	(" "),
	CASTLE 		("C");

	private final String name;
	

	private Tile(String name){
		
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	
	
}