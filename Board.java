import java.util.Random;

public class Board {
	
	//FIELDS
	private Tile[][] grid;
	private final int sizeGrid;
	private Random rng;
	
	//CONSTRUCTOR
	public Board() {
		
		this.rng = new Random();
		sizeGrid = 5;
		
		//set grid size (5*5)
		grid = new Tile[sizeGrid][sizeGrid];
		
		for (int i = 0; i<grid.length; i++) {
		
		//random grid[row][column] for hidden Wall
		int randomIndex = rng.nextInt(grid.length);
		
			for (int j = 0; j<grid[i].length; j++) {
				
				//set blank and hidden Wall
				if(j == randomIndex){
					grid[i][j] = Tile.HIDDEN_WALL;
				}
				else{
					grid[i][j] = Tile.BLANK;
				}
				
			}
			
		}
		
	}
	
	//TOSTRING
	public String toString() {
		
		String rows = "";
		
		for (int i = 0; i<grid.length; i++) {
			
			rows += "\n";
			
			for (int j = 0; j<grid[i].length; j++) {
				//rows += "______";
				
				//Display NAME ENUMS
				rows += (grid[i][j].getName() + " | ");
			}
			
		}
		
		return rows;
		
	}
	
	//GETTERS
	public int getSizeGrid(){
		return this.sizeGrid;
	}
	
	// INSTANCES METHODS
	
	public int placeToken(int row, int column) {
		
		
		if( (row <= grid.length && row >= 0) && (column <= grid.length && column >= 0) ) {
			
			//position castle and wall = try again
			if (grid[row][column].equals(Tile.CASTLE) || grid[row][column].equals(Tile.WALL) ) {

				return -1;
			}
			//position hiden wall = lost turn
			else if (grid[row][column].equals(Tile.HIDDEN_WALL) ){
				
				grid[row][column] = Tile.WALL;
				return 1;
			}
			//position blank = place castle + next turn
			else {
				
				grid[row][column] = Tile.CASTLE;
				return 0;
			}
	
		}
		else{
			return -2;
		}
		
	}
	
	
}